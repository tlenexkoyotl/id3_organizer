const fs = require('fs/promises');
const NodeID3 = require('node-id3')

const result = {
    names: []
}

const removeProblematicChars = string => string.replace(/[\/:"']/g, '_')

const main = async (sourcePath, destinationPath) => {
    try {
        const files = await fs.readdir(sourcePath);

        for (const file of files) {
            console.log(file);
            let {
                artist,
                album,
                disc,
                title
            } = NodeID3.read(`${sourcePath}/${file}`)
            artist = `${artist ? (artist.length > 0 ? artist : 'Unknown') : 'Unknown'}`
            artist = removeProblematicChars(artist)
            album = `${album ? (album.length > 0 ? album : artist) : artist}`
            album = removeProblematicChars(album)
            disc = `${disc ? (disc.length > 0 ? ` - ${disc}` : '') : ''}`
            disc = removeProblematicChars(disc)
            title = removeProblematicChars(title)
            result.names = [...result.names, {
                old: `${sourcePath}/${file}`,
                newPath: `${destinationPath}/${artist}/${album}/`,
                newFile: `${artist} - ${album}${disc} - ${title}`
            }]

            result.position = result.names.length - 1

            await fs.mkdir(result.names[result.position].newPath, {
                recursive: true
            })
            await fs.rename(result.names[result.position].old, `${result.names[result.position].newPath}${result.names[result.position].newFile}.mp3`)
        }

        return true
    } catch (err) {
        console.error(err);

        return false
    }
}

const musicFolder = '/Users/tlenexkoyotl/Music'

main(`${musicFolder}/Downloaded`, `${musicFolder}/Organized`).then(res => {
    console.log(res)
})